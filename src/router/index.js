import Vue from 'vue'
import Router from 'vue-router'
import IndexPage from '../components/index-page/IndexPage'
import SomePage from '../components/some-page/SomePage'
Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        name: 'IndexPage',
        component: IndexPage
      },
      {
          path: '/some',
          name: 'SomePage',
          component: SomePage
      }
    ]
})
