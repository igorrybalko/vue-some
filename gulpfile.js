//base part
let gulp = require('gulp'),
    rename  = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    imagemin = require('gulp-imagemin');

//css part
let sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer');

const buildFolder = 'static';

let pathFiles = {
    build : {
        css  : './' + buildFolder + '/css/',
        img : './' + buildFolder + '/images/',
    },
    src : {
        css  : './src/scss/**/*.scss',
        img : './src/images/**/*.*',
    }
};

function swallowError(error){
    console.log(error.toString());
    this.emit('end');
}

gulp.task('styles', function() {
    return gulp.src('./src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 10 versions', '> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(pathFiles.build.css));
});